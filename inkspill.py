import random, sys, webbrowser, copy, pygame
from pygame.locals import *

smallBoxSize = 60
smallBoardSize = 6
windowWidth = 640
windowHeight = 480
White = (255,255,255)
Black = (0,0,0)
Red = (255,0,0)
Green = (0,255,0)
Blue = (0,0,255)

Colorscheme = (White, Red, Green, Blue)
for i in range(len(Colorscheme)):
    assert len(Colorscheme[i]) == 4, 'Color scheme %s does not have exactly 4 colors.' % (i)
backGroundColor = Colorscheme[0]
paletteColors = Colorscheme[1:]
def main():
    global DISPLAYSURF
    lastPaletteClicked = None
    DISPLAYSURF = pygame.displacy.set((windowWidth, windowHeight))
    pygame.display.set_caption('Ink Flood')
    mousex = 0
    mousey = 0

    while True:
        paletteClicked = None
        resetGame = False
        checkForQuit
        DISPLAYSURF.fill(backGroundColor)

    if paletteClicked != None and paletteClicked != lastPaletteClicked:
        lastPaletteClicked = paletteClicked
        floodAnimation(mainBoard, paletteClicked)
        life -= 1
