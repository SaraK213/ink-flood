var numRows = 9;
var numCols = 9;
var startTable = new Array (numRows);
for (var row = 0; row < numRows; row++) {
  startTable[row] = new Array (numCols);
}

var ravenclaw = (0 , 0, 108)
var slytherin = (4, 62, 1)
var hufflepuff = (233, 172, 45)
var gryffindor = (174, 0, 1)
var hogwarts = (0, 0, 0)
var umbridge = (223, 144, 163)

var colors = "ravenclaw hogwarts gryffindor slytherin hufflepuff umbridge".split (/\s+/);

function year (difficulty) {
  if (difficulty == 'one') {
    numRows = 3;
    numCols = 3;
  } else if (difficulty == 'two') {
    numRows = 4;
    numCols = 4;
  } else if (difficulty == 'three') {
    numRows = 5;
    numCols = 5;
  } else if (difficulty == 'four') {
    numRows = 6;
    numCols = 6;
  } else if (difficulty == 'five') {
    numRows = 7;
    numCols = 7;
  } else if (difficulty == 'six') {
    numRows = 8;
    numCols = 8;
  } else if (difficulty == 'seven') {
    numRows = 9;
    numCols = 9;
  }
  clear (getById ("gameTableTbody"));
  createTable ();
}

function createNode (type, parent) {
  var newNode = document.createElement (type);
    parent.appendChild (newNode);
    return newNode;
  }

function appendText (parent, text) {
  var textNode = document.createTextNode (text);
  clear (parent);
  parent.appendChild(textNode);
}

function getById (id) {
  var element = document.getElementById (id);
  return element;
}

function clear (element) {
  while (element.lastChild)
    element.removeChild (element.lastChild);
}

var finished;

function floodElement (row, col, color) {
  gameTable[row][col].color = color;
  gameTable[row][col].element.className = "piece " + color;
}

function testColorFlood (row, col, color) {
  if (gameTable[row][col].flooded)
    return;
  if (gameTable[row][col].color == color) {
    gameTable[row][col].flooded = true;
    floodNeighbours (row, col, color);
  }
}

function floodNeighbours (row, col, color) {
    if (row < numRows - 1)
        testColorFlood (row + 1, col, color);
    if (row > 0)
        testColorFlood (row - 1, col, color);
    if (col < numCols - 1)
        testColorFlood (row, col + 1, color);
    if (col > 0)
        testColorFlood (row, col - 1, color);
}

function allFlooded () {
    for (var row = 0; row < numRows; row++) {
        for (var col = 0; col < numCols; col++) {
            if (! gameTable[row][col].flooded) {
                return false;
            }
        }
    }
    return true;
}
var win;
var goal = function(ending) {
  win = ending
}
//USE SCREENCAST-O-Matic

function flood (color, initial) {
  if (finished)
    return;
  var oldColor = gameTable[0][0].color;
  if (! initial && color == oldColor)
    return;

  for (var row = 0; row < numRows; row++)
    for (var col = 0; col < numCols; col++)
      if (gameTable[row][col].flooded)
        floodElement (row, col, color);

  for (var row = 0; row < numRows; row++)
    for (var col = 0; col < numCols; col++)
      if (gameTable[row][col].flooded)
        floodNeighbours (row, col, color);

  if (allFlooded()) {
    finished = true;
    if (color == win) {
      if (color == "ravenclaw") {
        alert ("Goal achieved! Ravenclaw has won the House Cup!!");
      } else if (color == "slytherin") {
        alert ("Goal achieved! Slytherin has won the House Cup!!");
      } else if (color == "gryffindor") {
        alert ("Goal achieved! Gryffindor has won the House Cup!!");
      } else if (color == "hufflepuff") {
        alert ("Goal achieved! Hufflepuff has won the House Cup!!");
      } else if (color == "hogwarts") {
        alert ("Goal achieved! Voldemort has defeated all of Hogwarts...");
      } else if (color == "umbridge"){
        alert ("You won... Umbridge has expelled Fred and George. You monster");
      }
    } else {
      alert ("You lose")
    }
  }
}

  function randomColor () {
    var colorNum = Math.floor (Math.random () * 6);
    return colors[colorNum];
  }
var gameTable = new Array (numRows);
  for (var row = 0; row < numRows; row++) {
    gameTable[row] = new Array (numCols);
    for (var col = 0; col < numCols; col++) {
      gameTable[row][col] = new Object ();
    }
  }

function createTable () {
  finished = false;
  var gameTableElement = getById ("gameTableTbody");
  for (var row = 0; row < numRows; row++) {
    var tr = createNode ("tr", gameTableElement);
    for (var col = 0; col < numCols; col++) {
      var td = createNode ("td", tr);
      var color = randomColor ();
      td.className = "piece " + color;
      gameTable[row][col].color = color;
      startTable[row][col] = color;
      gameTable[row][col].element = td;
      gameTable[row][col].flooded = false;
    }
  }
  gameTable[0][0].flooded = true;
  flood  (gameTable[0][0].color, true);
}
